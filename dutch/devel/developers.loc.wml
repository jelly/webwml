#use wml::debian::template title="Locatie van ontwikkelaars" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Waar zijn de Debian-ontwikkelaars (DD) gevestigd? Als een DD de coördinaten van zijn verblijfplaats heeft opgegeven in de ontwikkelaarsdatabase, dan is dat zichtbaar op onze wereldkaart.</p>
</aside>

<p>
De onderstaande kaart werd gegenereerd vanuit een geanonimiseerde <a
href="developers.coords">lijst van coördinaten van ontwikkelaars</a> met het
programma <a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.
</p>

<img src="developers.map.jpeg" alt="Wereldkaart">

<h2>Hoe u uw coördinaten kunt toevoegen</h2>

<p>
Indien u uw coördinaten zou willen toevoegen aan uw gegevens in de databank,
moet u inloggen op de <a href="https://db.debian.org">Database van Debian
ontwikkelaars</a> en uw gegevens aanpassen. Indien u de coördinaten van uw
woonplaats niet kent, dan kunt u <a href="https://www.openstreetmap.org">OpenStreetMap</a>
gebruiken om ze op te zoeken. Zoek naar uw stad en selecteer de richtingspijlen
naast het zoekveld. Sleep de groene markeerstift naar de OSM-kaart, en de
coördinaten verschijnen in het <em>Van</em>-veld.
</p>

<p>De opmaak van de coördinaten is in één van de volgende vormen:</p>

<dl>
  <dt>Decimale graden</dt>
    <dd>De opmaak is dan <code>+-DDD.DDDDDDDDDDDDDDD</code>. Programma's zoals
    Xearth en veel andere websites voor positiebepaling gebruiken die. De
    precisie ervan is beperkt tot 4 of 5 decimalen.</dd>
  <dt>Graden Minuten (DGM)</dt>
    <dd>De opmaak is dan <code>+-DDDMM.MMMMMMMMMMMMM</code>.  Het is geen
    rekenkundige, maar een gecomprimeerde weergave
    van twee afzonderlijke eenheden, graden en minuten.
    Deze schrijfwijze is gebruikelijk bij sommige types draagbare
    GPS-toestellen en bij GPS-berichten in de NMEA-opmaak.</dd>
  <dt>Graden Minuten Seconden (DGMS)</dt>
    <dd>De opmaak is dan <code>+-DDDMMSS.SSSSSSSSSSS</code>. Net als DGM is het
    geen rekenkundige, maar een gecomprimeerde weergave van drie afzonderlijke
    eenheden: graden, minuten en seconden. Deze schrijfwijze is typisch
    afgeleid van websites die 3 waarden opgeven voor elke positie. Bijvoorbeeld,
    <code>34:50:12.24523 Noord</code> zou een bepaalde positie kunnen zijn,
    en in DGMS zou dat dan <code>+0345012.24523</code> zijn.</dd>
</dl>

<p>
<strong>Merk op:</strong> <code>+</code> is het noorden voor de breedtegraad,
<code>+</code> is het oosten voor de lengtegraad. Indien uw positie minder
dan 2 graden verwijderd is van een nulpunt, is het van belang om
voldoende voorloopnullen te gebruiken opdat duidelijk zou zijn
welke opmaak van toepassing is.
</p>


#use wml::debian::template title="Espelhos do Debian (mundiais)" BARETITLE=true
#use wml::debian::translation-check translation="34422e94132789c2bcd0f6d41dc8680b49c7f3ab"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Espelhos do Debian por país</a></li>
  <li><a href="#complete-list">Lista completa de espelhos</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> O Debian é espelhado
(<a href="https://www.debian.org/mirror/">mirrored</a>) em centenas de
servidores. Se você planeja <a href="../download">baixar</a> o Debian, primeiro
tente um servidor mais próximo. Dessa forma provavelmente será mais rápido e
também reduzirá a carga em nossos servidores centrais.</p>
</aside>

<p class="centerblock">
  Os espelhos do Debian existem em muitos países, e para alguns deles,
  adicionamos um alias <code>ftp.&lt;country&gt;.debian.org</code>. Este alias
  geralmente aponta para um espelho que sincroniza regularmente e rapidamente,
  e carrega todas as arquiteturas do Debian. O repositório Debian está sempre
  disponível via HTTP localizado no diretório <code>/debian</code> no servidor.
</p>

<p class="centerblock">
  Outros sites de espelhos podem ter restrições sobre o que
  espelham (devido a restrições de espaço). Só porque um site não é o
  <code>ftp.&lt;country&gt;.debian.org</code> do país não significa
  necessariamente que seja mais lento ou menos atualizado do que o espelho
  <code>ftp.&lt;country&gt;.debian.org</code>.
  De fato, um espelho que carrega sua arquitetura e está mais próximo de
  você como usuário(a) e, portanto, mais rápido, é quase sempre preferível a
  outros espelhos que estão mais distantes.
</p>

<p>Utilize o site mais perto de você para baixar o mais rápido possível,
seja ele um alias do país ou não.
O programa
<a href="https://packages.debian.org/stable/net/netselect-apt">\
<em>netselect-apt</em></a> pode ser usado para determinar o site com menor
latência; utilize um programa de download como o
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> ou
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> para determinar o site com maior taxa de transferência.
Note que a proximidade geográfica geralmente não é o fator mais importante para
determinar qual máquina lhe servirá melhor.</p>

<p>
Se o seu sistema se movimenta muito, você poderá ser melhor servido(a) por um
"espelho" que é apoiado por uma
<abbr title="Content Delivery Network">CDN</abbr> global.
O projeto Debian mantém o domínio
<code>deb.debian.org</code> para este propósito e você pode usá-lo no seu
<code>sources.list</code> &mdash; consulte
<a href="http://deb.debian.org/">o site web</a> de serviços para detalhes.

<h2 id="per-country">Espelhos do Debian por país</h2>

<table border="0" class="center">
<tr>
  <th>País</th>
  <th>Site</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Lista completa de espelhos</h2>

<table border="0" class="center">
<tr>
  <th>Nome do host</th>
  <th>HTTP</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"

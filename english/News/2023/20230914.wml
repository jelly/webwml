<define-tag pagetitle>The Debian Project mourns the loss of Abraham Raji</define-tag>
<define-tag release_date>2023-09-14</define-tag>
#use wml::debian::news
# $Id$


<p>
The Debian Project has lost a member of its community. On 13th September
2023 Abraham Raji was involved in a fatal accident during a kayaking trip.
</p>

<p>
Abraham was a popular and respected Debian Developer as well a prominent free
software champion in his home state of Kerala, India. He was a talented graphic
designer and led design and branding work for DebConf23 and several other local
events in recent years. Abraham gave his time selflessly when mentoring new
contributors to the Debian project, and he was instrumental in creating and
maintaining the Debian India website.
</p>

<p>
The Debian Project honors his good work and strong dedication to Debian and
Free Software. Abraham’s contributions will not be forgotten, and the high
standards of his work will continue to serve as an inspiration to others.
</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce a completely free
operating system known as Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

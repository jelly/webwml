#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 9: выпуск 9.12</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о двенадцатом обновлении своего
предыдущего стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Исправления различных ошибок</h2>

<p>Данное обновление предыдущего стабильного выпуска вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction base-files "Обновление для текущей редакции">
<correction cargo "Новый выпуск основной версии разработки для поддержки обратного переноса Firefox ESR; исправление начальной загрузки для armhf">
<correction clamav "Новый выпуск основной версии разработки; исправление отказа в обслуживании [CVE-2019-15961]; удаление опции ScanOnAccess, заменённой на clamonacc">
<correction cups "Исправление проверки языка по умолчанию в ippSetValuetag [CVE-2019-2228]">
<correction debian-installer "Повторная сборка с учётом пакетов из oldstable-proposed-updates; установка gfxpayload=keep в подменю, что исправляет нечитаемые шрифты на дисплеях с высоким значением dpi при использовании образов сетевой загрузки с EFI; обновление значения USE_UDEBS_FROM по умолчанию с unstable на stretch, чтобы помочь пользователям выполнять локальные сборки">
<correction debian-installer-netboot-images "Повторная сборка с учётом пакетов из stretch-proposed-updates">
<correction debian-security-support "Обновление статуса поддержки безопасности некоторых пакетов">
<correction dehydrated "Новый выпуск основной ветки разработки; использование API ACMEv2 по умолчанию">
<correction dispmua "Новый выпуск основной ветки разработки, совместимый с Thunderbird 68">
<correction dpdk "Новый стабильный выпуск основной ветки разработки; исправление регрессии vhost, возникшей из-за исправления для CVE-2019-14818">
<correction fence-agents "Исправление неполного удаления fence_amt_ws">
<correction fig2dev "Разрешение текстовым строкам Fig v2 заканчиваться на несколько символов ^A [CVE-2019-19555]">
<correction flightcrew "Исправления безопасности [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Правильная обработка дельт в шрифтах TrueType GX, что исправляет отрисовку шрифтов с изменяемым хинтингом в Chromium и Firefox">
<correction glib2.0 "Проверка, чтобы клиенты libdbus могли аутентифицироваться с GDBusServer как и в ibus">
<correction gnustep-base "Исправление UDP-увеличения">
<correction italc "Исправления безопасности [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Пометка Международного дня детей (20 сентября) как выходного в Тюрингии с 2019 года и далее">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libidn "Исправление отказа в обслуживании в коде обработки Punycode [CVE-2017-14062]">
<correction libjaxen-java "Исправление ошибки сборки путём разрешения ошибок тестирования">
<correction libofx "Исправление разыменования NULL-указателя [CVE-2019-9656]">
<correction libole-storage-lite-perl "Исправление интерпретации годов с 2020 и далее">
<correction libparse-win32registry-perl "Исправление интерпретации годов с 2020 и далее">
<correction libperl4-corelibs-perl "Исправление интерпретации годов с 2020 и далее">
<correction libpst "Исправление определения get_current_dir_name и возврат досрочного завершения вычислений">
<correction libsixel "Исправление нескольких проблем безопасности [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Исправление переполнения буфера [CVE-2019-20387]">
<correction libtest-mocktime-perl "Исправление интерпретации годов с 2020 и далее">
<correction libtimedate-perl "Исправление интерпретации годов с 2020 и далее">
<correction libvncserver "RFBserver: не раскрывать стековую память удалённой системе [CVE-2019-15681]; разрешение заморозки во время закрытия соединения и ошибки сегментирования на  многопоточных VNC-серверах; исправление проблемы подключения к серверам VMWare; исправление аварийной остановки x11vnc при подключении vncviewer">
<correction libxslt "Исправление висячего указателя в xsltCopyText [CVE-2019-18197]">
<correction limnoria "Исправление удалённого раскрытия информации и возможного удалённого выполнения кода в дополнении Math [CVE-2019-19010]">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление с учётом ABI ядра Linux версии 4.9.0-12">
<correction llvm-toolchain-7 "Отключение компоновщика gold на s390x; начальная загрузка с -fno-addrsig, binutils в stretch не работает с этой опцией на mips64el">
<correction mariadb-10.1 "Новый стабильный выпуск основной ветки разработки [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Реализация независящего от позиции значения CSRF-куки">
<correction node-fstream "Стирание ссылки, если она находится на пути файла [CVE-2019-13173]">
<correction node-mixin-deep "Исправление загрязнения прототипа [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "Новый пакет для поддержки обратного переноса Firefox ESR">
<correction nvidia-graphics-drivers-legacy-340xx "Новый стабильный выпуск основной ветки разработки">
<correction nyancat "Повторная сборка в чистом окружении, чтобы добавить юнит systemd для nyancat-server">
<correction openjpeg2 "Исправление переполнения кучи [CVE-2018-21010], переполнения целых чисел [CVE-2018-20847] и деления на ноль [CVE-2016-9112]">
<correction perl "Исправление интерпретации годов с 2020 и далее">
<correction php-horde "Исправление межсайтового скриптинга в Horde Cloud Block [CVE-2019-12095]">
<correction postfix "Новый стабильный выпуск основной ветки разработки; обход плохой производительности TCP-интерфейса обратной петли">
<correction postgresql-9.6 "Новый выпуск основной ветки разработки">
<correction proftpd-dfsg "Исправление разыменования NULL-указателя в проверках CRL [CVE-2019-19269]">
<correction pykaraoke "Исправление пути к шрифтам">
<correction python-acme "Переход на протокол POST-as-GET">
<correction python-cryptography "Исправление ошибок тестирования при сборке с новыми версиями OpenSSL">
<correction python-flask-rdf "Исправление отсутствующих зависимостей в python3-flask-rdf">
<correction python-pgmagick "Обработка определения версии обновлений безопасности graphicsmagick, которые определяют себя как версию 1.4">
<correction python-werkzeug "Проверка того, чтобы контейнеры Docker имени уникальные отладочные PIN [CVE-2019-14806]">
<correction ros-ros-comm "Исправление переполнения буфера [CVE-2019-13566]; исправление переполнения целых чисел [CVE-2019-13445]">
<correction ruby-encryptor "Игнорирование ошибок тестирования, что исправляет ошибки сборки">
<correction rust-cbindgen "Новый пакет для поддержки обратных переносов Firefox ESR">
<correction rustc "Новая версия основной ветки разработки для поддержки обратных переносов Firefox ESR">
<correction safe-rm "Предотвращение установки в (и поломки) окружения с объединённым каталогом /usr">
<correction sorl-thumbnail "Обход исключения pgmagick">
<correction sssd "sysdb: очистка входных данных поискового фильтра [CVE-2017-12173]">
<correction tigervnc "Обновления безопасности [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Исправления безопасности [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper "Добавление <q>--protect '/tmp/systemd-private*/*'</q> к cron-задаче для предотвращения поломки служб systemd, имеющих директиву PrivateTmp=true">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction ublock-origin "Новая версия основной ветки разработки, совместимая с Firefox ESR68">
<correction unhide "Исправление исчерпания стека">
<correction x2goclient "Удаление ~/, ~user{,/}, ${HOME}{,/} и $HOME{,/} из целевых путей в режиме SCP; исправление регрессии, возникающей с новыми версиями libssh и исправлениями для CVE-2019-14889">
<correction xml-security-c "Исправление ошибки <q>Проверка DSA аварийно завершает OpenSSL при некорректной комбинации содержимого ключа</q>">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности предыдущего стабильного выпуска.
Команда безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction firetray "Не совместим с текущими версиями Thunderbird">
<correction koji "Проблемы безопасности">
<correction python-lamson "Сломан из-за изменений в python-daemon">
<correction radare2 "Проблемы безопасности; основная ветка разработки не предоставляет стабильную поддержку">
<correction ruby-simple-form "Не используется; проблемы безопасности">
<correction trafficserver "Не может поддерживаться">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий предыдущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Предлагаемые обновления для предыдущего стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Информация о предыдущем стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>

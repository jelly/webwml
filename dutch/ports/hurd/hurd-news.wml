#use wml::debian::template title="Debian GNU/Hurd -- Nieuws" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="5dc8f3e72bae7835b0a6f4dc178b5501f5c08d4e"

<h1>Nieuws over Debian GNU/Hurd</h1>

<h3><:=spokendate ("2023-06-10"):></h3>

<p>Debian GNU/Hurd 2023 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de <strong>release
van Debian GNU/Hurd 2023</strong> aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de stabiele release
van Debian "Bookworm" (juni 2023). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/12.0/hurd-i386/">cdimage</a>
in de Debian varianten: NETINST en netboot. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen. De makkelijkste manier om dit te doen is
<a href="https://www.debian.org/ports/hurd/hurd-install">binnenin een virtuele
machine zoals qemu</a></p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 65% van het Debian-archief, en er is nog meer op komst!</p>

<ul>
<li>De ondersteuning voor APIC, SMP en 64bits is sterk verbeterd: er wordt nu een compleet Debian-systeem opgestart, maar er moeten nog enkele bugs worden opgelost.
</li>
<li>Het op rump gebaseerde schijfstuurprogramma in gebruikersland lijkt nu goed te werken, we kunnen nu een systeem opstarten zonder enig Linux stuurprogramma in de mach kernel: mach stuurt dan alleen de CPU, het geheugen, de klok en de irqs aan!</li>
<li>Veel verbeteringen, waaronder enkele belangrijke veiligheidsverbeteringen.</li>
</ul>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a> (of <a href="http://darnassus.sceen.net/~hurd-web/faq/">zijn recentste versie</a>),
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de vertaler (translator)</a>
om de geweldige mogelijkheden van GNU/Hurd grondig te leren kennen.</p>

<p>We willen alle mensen bedanken die
<a href=https://www.gnu.org/software/hurd/history.html>in het verleden</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2021-08-14"):></h3>

<p>Debian GNU/Hurd 2021 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de <strong>release
van Debian GNU/Hurd 2021</strong> aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de stabiele release
van Debian "Bullseye" (augustus 2021). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.
</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/11.0/hurd-i386/">cdimage</a>
in de drie voor Debian gebruikelijke varianten: NETINST, cd en dvd. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen. De makkelijkste manier om dit te doen is
<a href="https://www.debian.org/ports/hurd/hurd-install">binnenin een virtuele
machine zoals qemu</a></p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 70% van het Debian-archief, en er is nog meer op komst!</p>

<ul>
<li>De omzetting van go is voltooid</li>
<li>Ondersteuning voor het vergrendelen van bestandsregisters werd toegevoegd</li>
<li>Sommige onderdelen van een experimentele APIC-, SMP- en 64bits-ondersteuning zijn toegevoegd</li>
<li>Het leveren van IRQ's in gebruikersland werd herwerkt</li>
<li>Een experimenteel op rump gebaseerd schijfstuurprogramma in gebruikersland werd geïntroduceerd. Dit betekent dat het weglaten van de Linux-lijm uit de GNU Mach-kernel heel dichtbij komt!</li>
<li>Veel verbeteringen, waaronder enkele belangrijke veiligheidsverbeteringen.</li>
</ul>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a> (of <a href="http://darnassus.sceen.net/~hurd-web/faq/">zijn recentste versie</a>),
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de translator (vertaler)</a>
om de geweldige mogelijkheden van GNU/Hurd te leren kennen.</p>

<p>We willen alle mensen bedanken die
<a href=https://www.gnu.org/software/hurd/history.html>in het verleden</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2019-07-07"):></h3>

<p>Debian GNU/Hurd 2019 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de <strong>release
van Debian GNU/Hurd 2019</strong> aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de stabiele release
van Debian "buster" (juli 2019). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/10.0/hurd-i386/">cdimage</a>
in de drie voor Debian gebruikelijke varianten: NETINST, cd en dvd. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen. De makkelijkste manier om dit te doen is
<a href="https://www.debian.org/ports/hurd/hurd-install">binnenin een virtuele
machine zoals qemu</a></p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 80% van het Debian-archief, en er is nog meer op komst!</p>

<ul>
<li>Er is een ACPI-vertaler beschikbaar; deze wordt momenteel
alleen gebruikt om het systeem af te sluiten.</li>
<li>De LwIP TCP/IP-stapel is nu beschikbaar als optie.</li>
<li>Er werd een PCI-arbiter geïntroduceerd, die nuttig zal zijn om PCI-toegang op de juiste manier te beheren en om fijnmazige hardwaretoegang te bieden.</li>
<li>Ondersteuning voor LLVM werd geïntroduceerd.</li>
<li>Nieuwe optimalisaties omvatten beveiligde payloads, beter paging-beheer en
berichtverzending, en gsync-synchronisatie.</li>
</ul>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a> (of <a href="http://darnassus.sceen.net/~hurd-web/faq/">zijn recentste versie</a>),
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de vertaler (translator)</a>
om de geweldige mogelijkheden van GNU/Hurd te leren kennen.</p>

<p>We willen alle mensen bedanken die
<a href=https://www.gnu.org/software/hurd/history.html>in het verleden</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2017-06-18"):></h3>

<p>Debian GNU/Hurd 2017 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de
<strong>release van Debian GNU/Hurd 2017</strong>  aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de stabiele release
van Debian "stretch" (mei 2017). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/9.0/hurd-i386/">cdimage</a>
in de drie voor Debian gebruikelijke varianten: NETINST, cd en dvd. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen. De makkelijkste manier om dit te doen is
<a href="https://www.debian.org/ports/hurd/hurd-install">binnenin een virtuele
machine zoals qemu</a></p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 80% van het Debian-archief, en er is nog meer op komst!</p>

<ul>
<li>De kernpakketten GNU Hurd en GNU Mach zijn respectievelijk bijgewerkt naar
    versies 0.9 en 1.8. Naast talrijke andere verbeteringen, brengen ze een
    sterk verbeterde stabiliteit onder geheugenbelasting en een verlengde
    bedrijfstijd.</li>
<li>Het eigen fakeroot-gereedschap werd sterk verbeterd, zodat het nu ook
    gebruikt kan worden voor het bouwen van pakketten, waardoor dat veel
    sneller en veiliger gaat.</li>
<li>Het is nu mogelijk om subhurds als niet-geprivilegieerde gebruiker uit te
    voeren, waardoor een gemakkelijke lichtgewicht virtualisatie wordt
    geboden.</li>
<li>De ondersteunde geheugengrootte werd uitgebreid tot meer dan 3GiB.</li>
</ul>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a> (of <a href="http://darnassus.sceen.net/~hurd-web/faq/">zijn recentste versie</a>),
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de vertaler (translator)</a>
om de geweldige mogelijkheden van GNU/Hurd te leren kennen.</p>

<p>We willen alle mensen bedanken die
 <a href=https://www.gnu.org/software/hurd/history.html>in het verleden</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2015-04-25"):></h3>

<p>Debian GNU/Hurd 2015 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de
<strong>release van Debian GNU/Hurd 2015</strong> aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de stabiele release
van Debian "jessie" (april 2015). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/8.0/hurd-i386/">De omzettingen van Debian (Debian ports)</a>
in de drie voor Debian gebruikelijke varianten: NETINST, cd en dvd. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen. De makkelijkste manier om dit te doen is
<a href="https://www.debian.org/ports/hurd/hurd-install">binnenin een virtuele
machine zoals qemu</a></p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 80% van het Debian-archief, en er is nog meer op komst!</p>

<p>Sinds de laatste momentopname-release die samenviel met "wheezy", werd
overgeschakeld naar sysvinit als init-systeem voor een meer bij Debian
aansluitende beleving. Andere wijzigingen sinds de laatste momentopname zijn
onder meer:</p>

<ul>
<li>De kernpakketten GNU Hurd en GNU Mach zijn respectievelijk bijgewerkt naar
    versies 0.6 en 1.5. Naast talrijke andere verbeteringen, brengen ze een
    sterk verbeterde stabiliteit onder belasting en een verlengde
    bedrijfstijd.</li>
<li>Voor de netwerkstuurprogramma's werd overgestapt naar stuurprogramma's in
    de gebruikersruimte met behulp van het NetDDE-raamwerk en een basis van
    Linux-2.6.32-code.</li>
</ul>

<p>
Opmerkelijke nieuwe of opgewaardeerde pakketten die een aanzienlijke
overdrachtsinspanning vergden en/of waarvan bekend is dat ze goed werken op
Debian GNU/Hurd zijn Iceweasel 31 ESR, Xfce 4.10,
X.org 7.7 en Emacs 24.4.
</p>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a> (of <a href="http://darnassus.sceen.net/~hurd-web/faq/">zijn recentste versie</a>),
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de vertaler (translator)</a>
om de geweldige mogelijkheden van GNU/Hurd te leren kennen.</p>

<p>We willen alle mensen bedanken die
<a href=https://www.gnu.org/software/hurd/history.html>in het verleden</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2013-05-21"):></h3>

<p>Debian GNU/Hurd 2013 <em>uitgebracht</em>!</p>

<p>Het is met groot genoegen dat het Debian GNU/Hurd-team de
<strong>release van Debian GNU/Hurd 2013</strong> aankondigt. <br />
Het is een momentopname van Debian "sid" op het moment van de release
van Debian "wheezy" (mei 2013). Ze is dus grotendeels gebaseerd op
dezelfde bronnen. Het is <em>geen</em> officiële Debian-release, maar het is
een officiële release van het geschikt maken van Debian (een Debian port) voor
Debian GNU/Hurd.</p>

<p>De ISO-installatie-images kunnen gedownload worden van
<a href="https://cdimage.debian.org/cdimage/ports/7.0/hurd-i386/">De omzettingen van Debian (Debian ports)</a>
in de drie voor Debian gebruikelijke varianten: NETINST, cd en dvd. Naast het
gebruiksvriendelijke installatieprogramma van Debian is er ook een vooraf
geïnstalleerd schijfimage beschikbaar, waardoor het nog gemakkelijker wordt om
Debian GNU/Hurd uit te proberen.</p>

<p>Debian GNU/Hurd is momenteel beschikbaar voor de i386-architectuur met
ongeveer 75% van het Debian-archief, en er is nog meer op komst!</p>

<p>Zorg ervoor dat u de
<a href="https://www.debian.org/ports/hurd/hurd-install">configuratie-informatie</a> leest,
alsook de <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a>,
en de <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">beknopte handleiding over de vertaler (translator)</a>
om de geweldige mogelijkheden van GNU/Hurd te leren kennen.</p>

<p>Door het zeer kleine aantal ontwikkelaars is de voortgang van het project
niet zo snel gegaan als bij andere succesvolle besturingssystemen, maar wij
menen <a href="https://www.gnu.org/software/hurd/hurd/status.html">een zeer
behoorlijke toestand</a> te hebben bereikt, zelfs met onze beperkte middelen.</p>

<p>We willen alle mensen bedanken die <a
href=https://www.gnu.org/software/hurd/history.html>de afgelopen decennia</a>
aan GNU/Hurd hebben gewerkt. Op geen enkel moment waren het er veel (en vandaag
nog steeds niet, <a href="https://www.gnu.org/software/hurd/contributing.html">doe alsjeblieft mee</a>!),
maar uiteindelijk hebben veel mensen op de een of andere manier bijgedragen.
<strong>Bedankt iedereen!</strong></p>

<h3><:=spokendate ("2011-06-11"):></h3>

<p>Verschillende bugs van het op het debian-installatiesysteem gebaseerde image
zijn verholpen. Er is geen bekend probleem meer, behalve dat GNOME en KDE nog
niet kunnen worden geïnstalleerd.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2011-02-15"):></h3>

<p>Het op het debian-installatiesysteem gebaseerd image werd opgewaardeerd
naar squeeze-pakketten van het debian-installatiesysteem.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2010-09-01"):></h3>

<p>Een op het debian-installatiesysteem gebaseerd image is beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2009-10-19"):></h3>

<p>De L1 dvd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2007-12-21"):></h3>

<p>De K16 cd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2007-11-19"):></h3>

<p>De K15 cd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2006-11-27"):></h3>

<p>De K14 cd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2006-04-26"):></h3>

<p>Het K11 mini cd-image is nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2005-10-26"):></h3>

<p>The K10 cd- en dvd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2005-05-14"):></h3>

<p>De K9 cd-images zijn nu beschikbaar.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2004-12-30"):></h3>

<P>De K8 cd-images zijn nu beschikbaar. Deze images ondersteunen
bestandssystemen groter dan 2 GB en bevatten een update van de stuurprogramma's
voor netwerkapparaten.<br/>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2004-03-06"):></h3>

<P>Na een lange tijd niet te zijn bijgewerkt, werden nieuwe CVS-momentopnames
van <a href="https://tracker.debian.org/pkg/hurd">de Hurd</a> en
<a href="https://tracker.debian.org/pkg/gnumach">GNU Mach</a> geüpload.</p>

<h3><:=spokendate ("2003-07-31"):></h3>

<P>De K4 cd-images zijn nu beschikbaar.
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2003-04-30"):></h3>

<P>De K3 cd-images zijn nu beschikbaar.
Ze zijn hernoemd naar GNU-K3-CDx.iso <br>
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>


<h3><:=spokendate ("2003-03-06"):></h3>

<P>De K2 cd-images zijn nu beschikbaar.
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2002-10-10"):></h3>

<P>De J2 cd-images zijn nu beschikbaar.
Zie de <a href="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>


<P>Opwaarderen van Debian GNU/Hurd vanaf een op libio gebaseerd systeem van voor
12-08-2002 (met inbegrip van de J1 cd-reeks).

<P>Een Debian GNU/Hurd systeem bijwerken in augustus 2002 vereist het volgen
van de procedure die geschetst wordt in de
<A HREF="extra-files/hurd-upgrade.txt">opwaarderingshandleiding</A>. Deze
bijwerkingsprocedure is nodig omdat de Hurd-interfaces een onverenigbare
wijziging hebben ondergaan om de ondersteuning van lange bestanden voor te
bereiden.

<h3><:=spokendate ("2002-08-05"):></h3>

<P>De J1 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2002-02-26"):></h3>

<P>De H3 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2001-12-15"):></h3>

<P>De H2 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2001-11-11"):></h3>

<P>De H1 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2001-10-05"):></h3>

<P>De G1 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2001-08-07"):></h3>

<p>
Vandaag hebben we voor het eerst de kaap van 40% overschreden in de <a
href="https://buildd.debian.org/stats/graph.png">statistiek over bijgewerkte
pakketten</a> per architectuur.</p>

<h3><:=spokendate ("2001-07-31"):></h3>

<P>De F3 cd-images zijn nu beschikbaar.
Zie de <A HREF="./hurd-cd">pagina over de Hurd-cd</a> voor verdere informatie.</p>

<h3><:=spokendate ("2001-07-12"):></h3>

<p>Marcus Brinkmann heeft zijn presentatie over de Hurd beschikbaar gesteld.
Ze is momenteel beschikbaar op:</p>
<a href="http://www.marcus-brinkmann.de/talks.en.html">http://www.marcus-brinkmann.de/talks.en.html</a>

<h3><:=spokendate ("1999-11-01"):></h3>

<p>
Enkele openstaande bugrapporten opgeschoond die inmiddels gesloten waren. Ik ga
nog wat pakketten uploaden, naast de pakketten die de afgelopen dagen zijn
geüpload (inetutils, grub, man-db; nu komen hostname, mtools, ...). Al deze
pakketten kunnen nu zonder enige wijziging gecompileerd worden, wat een goede
zaak is. Het lijkt erop dat we langzaam de basisset van pakketten
stabiliseren.</p>

<h3><:=spokendate ("1999-09-29"):></h3>

<p>
Er zijn nu <a href="ftp://alpha.gnu.org/gnu/hurd/contrib/marcus/">patches om
de lettertekengeoriënteerde apparaten van Linux in GNU Mach te lijmen</a>. Deze
patches zijn erg experimenteel en het tty-stuurprogramma werkt niet correct met
de term-vertaler, maar we werken eraan en hopen binnenkort een
binair bestand te kunnen leveren. Merk op dat dit de Linux-console naar de Hurd
zal brengen (inclusief kleuren- en virtuele consoles), evenals stuurprogramma's
voor verschillende niet-standaard muizen en andere seriële apparaten.</p>

<p>
Ik heb gehoord dat sommige mensen zich zorgen maken over de grootte van GNU
Mach en dat het een onderdeel van Linux wordt. Houd er rekening mee dat we hier
alleen naar een tijdelijke oplossing zoeken, totdat we de tijd hebben om de
stuurprogramma-interface in GNU Mach opnieuw te ontwerpen (of een andere
Microkernel te gebruiken). De microkernel is helemaal niet zo belangrijk als de
Hurd-servers die er bovenop actief zijn.</p>

<p>
Wat pakketten betreft, hebben we nu een goed <code>shadow</code>-pakket
(dat <code>passwd</code> uitvoert (dank u, BenC!)). Ook
<code>man-db</code> zou nu automatisch moeten werken, zelfs met lange
bestandsnamen, maar de andere aanpassingen heb ik niet gecontroleerd. Al bij al
geraakt het basisgedeelte van pakketten in een goede staat. Torin heeft mijn
patch voor <code>perl</code> toegepast en dat is nog een pakket waarvan ik moet
nagaan of het nu 'afgerond' kan worden.</p>

<h3><:=spokendate ("1999-08-31"):></h3>

<p>
<code>debianutils 1.12</code> kan nu zonder patch gecompileerd worden.</p>

<h3><:=spokendate ("1999-08-05"):></h3>

<p>
Een broncode-NMU voor <code>passwd</code> (<code>shadow</code>) zou alle
resterende problemen in dit pakket moeten oplossen. Dit zou moeten zorgen voor
een vlottere installatie. Anderzijds is een kleine patch nodig voor
<code>mutt</code>.</p>

<h3><:=spokendate ("1999-07-27"):></h3>

<p>
Er zijn nu nieuwe pakketten van het kernsysteem afgewerkt. De Hurd heeft een
nieuwe opstartmethode (het deel dat plaats vindt nadat init opgestart is). Kijk
eens naar <code>/libexec/runsystem</code>. Dit is nuttig voor het
<code>sysvinit</code>-pakket, dat bijna klaar is. Een klein bugje werd opgelost
in GNU Lib C en een in GNU Mach. Automatische detectie van alle netwerkkaarten
zou nu moeten werken, maar misschien moeten we de volgorde een beetje aanpassen
(3c5x9 voor 3c59x).</p>

<h3><:=spokendate ("1999-07-22"):></h3>

<p>
In <code>cpio 2.4.2-25</code> werd het resterende Hurd-compatibiliteitsprobleem
opgelost en dit kan nu zonder wijzigingen worden gecompileerd. Bedankt Brian!</p>

<h3><:=spokendate ("1999-07-05"):></h3>

<p>
Patches voor Perl 5.005.03 werden bij de beheerder ingediend. De bovenstroomse
code was al zuiver (bedankt, Mark!), maar de verpakkingsscripts in Debian waren
Linux-specifiek.</p>

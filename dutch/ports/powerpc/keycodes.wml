#use wml::debian::template title="PowerPC: aanpassing van de toetsencodes voor toetsenborden" NOHEADER="yes"
#use wml::fmt::verbatim
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::translation-check translation="0cfff43768945b514bb734757927bbbd8b043626"


<h2>Waarschuwing in verband met de toetsencodes van het toetsenbord voor PowerPC Linux!</h2>
<p>
Als u een kernel gebruikt die vóór april 2001 is gebouwd en een opwaardering naar woody, een kernelupgrade of een wijziging van de toetsenbordindeling overweegt, is het ESSENTIEEL dat u op de hoogte bent van de wijziging in Debian van de toetsencodes voor PowerPC in woody.
</p>
<p>
Dit is wat er gebeurde: om powerpc aan te passen aan de reguliere werkwijze voor linux en huidige en toekomstige conflicten te elimineren, werd het pakket console-data in woody aangepast om voortaan voor installatie de toetsenbordindelingen van linux te gebruiken in plaats van de ADB-toetsenbordindelingen (wat vroeger de norm was voor powerpc-kernels). De ADB-toetsenbordindelingen worden niet langer officieel ondersteund door Debian.
</p>
<p>
De kernel van het installatiesysteem werd aan deze ontwikkeling aangepast. Dus maakt de nieuwe installatiekernel gebruik van linux-toetsencodes, niet van ADB-toetsencodes. Dat is een permanente verandering; een kernel gecompileerd zonder ondersteuning voor ADB-toetsencodes is niet in staat om ADB-toetsenbordindelingen te gebruiken. Linux-toetsencodes zijn een functionaliteit van de "New Input Layer" (nieuwe invoerlaag) die is ingesteld om de standaard te worden voor alle invoerapparaten op alle architecturen tijdens de versiecyclus volgend op kernel 2.4.
</p>
<p>
Als u voorlopig liever bij ADB-toetsencodes blijft en de kernel die u wilt gebruiken gecompileerd is met CONFIG_MAC_ADBKEYCODES=n, kunt u deze opnieuw compileren met CONFIG_MAC_ADBKEYCODES=y en een ADB-toetsenbordindeling blijven gebruiken.
</p>
<p>
De toetsenbordindelingen van mac/apple voor ADB-toetsencodes zijn heel verschillend van linux-toetsencodes. Daarom moet u voorbereid zijn. Als u uw opstartinstructies, uw kernel en uw toetsenbordindeling niet op elkaar afstemt, kunt u op een dag 'root' typen aan de inlogprompt en in plaats daarvan 'sswj' te zien krijgen. Een zeer verontrustende situatie.
</p>


<h2>Vooruitplannen</h2>
<p>
Een veel voorkomende situatie waarin dit kan gebeuren is bij het opwaarderen van potato naar woody. Voor deze situatie is bij de opwaardering van console-data een speciaal dialoogvenster opgenomen. Er wordt een controle uitgevoerd van de kernel die u gebruikt wanneer de opwaardering plaatsvindt, en als u een ADB-kernel gebruikt, wordt u gewaarschuwd voor deze situatie en gevraagd om een keuze te maken.
</p>
<p>
Aangezien u al alles over het probleem weet, is de keuze gemakkelijk. Installeer een nieuwe opstart-toetsenbordindeling en pas nadat de upgrade is voltooid (VOOR de computer opnieuw opgestart wordt) uw opstartconfiguratie aan (quik.conf of yaboot.conf) door aan de sectie over de kernel-image waarmee u opstart de volgende regel toe te voegen:
</p>
<pre>
append="keyboard_sends_linux_keycodes=1"
</pre>
<p>
Als u al een regel heeft met append=, voeg dan de nieuwe frase toe binnen de aanhalingstekens zoals</p>
<pre>
append="video=ofonly keyboard_sends_linux_keycodes=1"
</pre>
<p>
Vergeet niet om quik of ybin uit te voeren nadat uw bewerking is voltooid, om de configuratiewijzigingen op te slaan in de echte bootloader-bestanden.
</p>
<p>
Een andere situatie waarin dit probleem kan optreden, is bij het opwaarderen van X van 4.0.x naar 4.1.x met een kernel met ADB-toetsencodes. In uw XF86Config-4 staat waarschijnlijk XkbModel ingesteld op "macintosh", maar de betekenis daarvan is veranderd, zodat wordt aangenomen dat een macintosh-toetsenbord de nieuwe Linux-toetsencodes gebruikt. Als u bij ADB-toetsencodes blijft, moet u het XkbModel wijzigen in "macintosh_old".
</p>

<h2>Nieuwe Woody-installaties</h2>
<p>
Bij een nieuwe woody-installatie worden een toetsenbordindeling met linux-toetsencodes en een kernel met linux-toetsencodes voor u geïnstalleerd. Dus ze passen bij elkaar en u zult geen enkel probleem hebben. Tenzij...
</p>
<p>
Tenzij u na de installatie de kernel wijzigt en dit een ADB-kernel blijkt te zijn. Dan heeft u hetzelfde probleem in de omgekeerde richting. Of tenzij...
</p>
<p>
Tenzij u de toetsenbordindeling handmatig wijzigt door er een te selecteren uit /usr/share/keymaps/mac. Dat zijn allemaal ADB-toetsenbordindelingen en ze komen niet overeen met uw kernel met linux-toetsencodes!
</p>


<h2>Mij zal het niet overkomen -- maar wanneer het toch gebeurt</h2>
<p>
Dus, hoe redt u uzelf uit een situatie waarin u root typt en sswj ziet? Of in het omgekeerde geval, als u (tab)ssw typt en root ziet?</p>
<p>
Linux-gebruikers houden er niet van om de schakelaar van hun computer uit te zetten. En er treedt altijd een zekere beschadiging van het bestandssysteem op als u dat doet, die al dan niet correct gerepareerd kan worden. Dus hier volgen enkele suggesties om het systeem netjes af te sluiten als dit gebeurt.</p>
<p>
Als u ssh op uw systeem hebt geïnstalleerd en vanaf een andere computer verbinding kunt maken, kunt u het probleem tijdelijk op afstand verhelpen. Log in op de root-account en voer het volgende uit:
</p>

<verbatim>
cd /proc/sys/dev/mac_hid/
echo 0 > keyboard_lock_keycodes
echo 1 > keyboard_sends_linux_keycodes
</verbatim>

<p>
Uw toetsenbord zal dan normaal reageren totdat u opnieuw opstart. Maak van de gelegenheid gebruik om uw toetsenbordindeling, kernel en bootloader op elkaar af te stellen!</p>
<p>
Als op uw systeem een toetscombinatie gebruikt wordt voor de commando's opnieuw opstarten en afsluiten, kunt u proberen de onderstaande tabellen te gebruiken om erachter te komen wat de combinatie is en deze toepassen. Op PowerPC is een daarvoor vaak gebruikte toetsencombinatie Control-Shift-Delete. Met ADB-toetsencodes die geïnterpreteerd worden als linux-toetsencodes, zou dat dan Control-F6-F12 worden. Met linux-toetsencodes die geïnterpreteerd worden door een kernel geladen met ADB-toetsencodes, zou u Shift-AltGr-Gelijkheidsteken moeten gebruiken. Veel succes.
</p>
<p>
Mijn toetsenbord heeft geen Delete-toets. Dus toen ik linux-toetsencodes had geladen in een voor ADB geconfigureerde kernel, zocht ik aan de hand van de tabel uit hoe ik kon inloggen als root ( 2==3 gevolgd door F5 ). Voor mijn wachtwoord als root gebruikte ik de onderstaande tabel. Voor het opnieuw opstarten typte ik ( 21 tab-toets ==3 gevolgd door F5 ). U kunt ook halt gebruiken ( p]j3 gevolgd door F5 ).
</p>
<p>
Sommige tekens kunnen niet in de verkeerde toetsenbordindeling worden getypt. Die zijn blanco of ontbreken in de tabellen.</p>

<h2>Toetsenbordindeling met Linux-toetsencodes geladen, kernel geconfigureerd voor ADB</h2>

<p>&nbsp;</p>

<pre>
Als u wenst:  a b   c d e f g h i j      k l m n      o p q r s t u v w
type dan:     ] TAB m u 1 [ i p 5 Return l j ` Spatie = 9 y 2 o 3 6 . t

               x y z 0 1 2 3 4 5 6 7 8 9 *   /  [ ] ,         = - ` \ ;
               n 4 / b d f h g z x c v   Alt F7 7 - Backspace w q ; , '

              Control Shift     Enter Tab Backspace Fwd-Del Spatie
              F6      Ctrl or \ F5, 8 r   e         F12     CapsLock

              Home   NumLock   Clear  AltGr =(numkpd) Escape F11 F12
              Clear  +(numkpd) F6-6   Shift Fwd-Del   s      kp-5 kp-6

              F1         F2          F3         F4       F7   F9
              Pijl-links Pijl-rechts Pijl-neer  Pijl-op  kp-. kp-*

              Pijl-links Pijl-rechts Pijl-op Pijl-neer
              F13                    F11
</pre>

<h2>Kernel geconfigureerd voor Linux-toetsencodes, ADB-toetsenbordindeling geladen</h2>

<pre>
Als u wenst:  a b c d e         f g h i j k l m n o p q r   s   t u v w
type dan:       0 7 1 Backspace 2 4 3 g l ' k c x s h - Tab Esc w d 8 =

              x y z 0       1 2 3 4 5 6 7 8      9 * /  [ ] , = - ` ;
              6 q 5 Control e r t y i u [ Return p   z  f a \ o ] m `

              Control Shift Return Tab Backspace Fwd-Del   \     Spatie
              Shift   AltGr j      b   ,         =(numkpd) Shift n

              Clear  AltGr    =(numkpd) CapsLock Escape Alt
              Home   CapsLock Fwd-Del   Space    /       kp-*

              F1 F2 F3 F4 F5    F6   F7 F8 F9 F0 F11 F12
                          Enter Ctrl /               Fwd-Del

              Pijl-links Pijl-rechts Pijl-op  Pijl-neer
              F1         F2          F4       F3
</pre>


<h2>Hoe u uw huidige situatie kunt achterhalen</h2>
<p>
De toetsenbordindelingen bevatten momenteel geen commentaar, dus als u zich afvraagt welk type indeling actief is, kunt u dat zien door naar de regel met toetsencode 1 regel te kijken met</p>
<pre>
zgrep 'keycode *1 =' /etc/console/boottime.kmap.gz
</pre>
Als toetsencode 1 = Escape, dan is dat de indeling met linux-toetsencodes (eigenlijk i386).
ls toetsencode 1 = s, dan is dat ADB (behalve vooor ADB dvorak, toetsencode 1 = o).
<p>
De config-XXXXX-bestanden in /boot laten zien of de kernel waarmee u opstart gecompileerd is met ondersteuning voor ADB-toetsencodes of niet. Om daar achter te komen, gebruikt u
</p>
<pre>
grep MAC_ADB /boot/*
</pre>
<p>
U zou een lijst moeten krijgen met configuratiebestanden voor kernels die u kunt opstarten. Als er een vermelding is zoals
</p>
<pre>
/boot/config-2.4.12-powerpc:CONFIG_MAC_ADBKEYCODES=y
</pre>
<p>dan is uw kernel gecompileerd met ondersteuning voor ADB-toetsencodes. Als de laatste letter een n is, dan is het een kernel met linux-toetsencodes.</p>

<h2>Hoe u dit kunt oplossen</h2>
<p>
Als uw defecte systeem eenmaal is afgesloten, moet u het nog steeds repareren. Hoe weet u wat er nodig is? Het kan zijn dat u uw hersteldiskette moet gebruiken of een andere partitie moet opstarten om de boel te repareren.
</p>
<p>
Als uw probleem een voor ADB gecompileerde kernel is die probeert een toetsenbordindeling met linux-toetsencodes te gebruiken, voeg dan gewoon het volgende toe
If your problem is an ADB-compiled kernel trying to use a linux-codes
keymap, just add </p>
<pre>
keyboard_sends_linux_keycodes=1
</pre>
<p>
aan de boot: prompt na het typen van uw kernel-imagelabel. Dat is echter slechts een eenmalige oplossing. U moet de wijziging permanent maken door uw opstartconfiguratiebestand te bewerken en op te slaan voor de bootloader.
</p>
<p>
Als uw probleem het omgekeerde is (linux-toetsencodes-kernel probeert een ADB-toetsenbordindeling te gebruiken), moet u de ADB-toetsenbordindeling verwijderen. U kunt elke toetsenbordindeling uit de map /usr/share/keymaps/i386 kopiëren, er zijn er veel om uit te kiezen. Bijvoorbeeld
</p>
<pre>
cd /usr/share/keymaps/i386/qwerty/
cp mac-usb-us.kmap.gz /etc/console/boottime.kmap.gz
</pre>

#use wml::debian::template title="Debian &ldquo;buster&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7730de84fec07cc853e4d91fab127a0371a9067b"

<p>Debian <current_release_buster> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 was initially released on <:=spokendate('2019-07-06'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen,
beschreven in ons
<a href="$(HOME)/News/2019/20190706">persbericht</a> en de
the <a href="releasenotes">Notities bij de release</a>.</p>

<p><strong>Debian 10 werd vervangen door
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds
<:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Buster geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot 30 juni, 2024. De LTS is beperkt tot i386, amd64, armel, armhf en arm64.
Alle andere architecturen worden niet langer ondersteund in buster.
Raadpleeg voor meer informatie de <a
href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
</strong></p>

<p>Raadpleeg de instructies in de
<a href="releasenotes">Notities bij de release</a> om van een oudere Debian
release op te waarderen..</p>

<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/arm64/">64-bits ARM (AArch64)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>


<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd <a href="../reportingbugs">andere problemen rapporteren</a>.</p>

#use wml::debian::ddp title="Debian 开发者手册"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"

<document "Debian 政策手册" "policy">

<div class="centerblock">
<p>
该手册描述了 Debian GNU/Linux 发行版的政策要求。这包括了 Debian
归档的结构和内容、一些操作系统设计事项以及每个软件包在被收入\
发行版时必须满足的技术要求。

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "Debian Plicy 团队">
  <status>
  已完成
  ready
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p>政策文档<a href="https://bugs.debian.org/debian-policy">提议修订的讨论</a>

  <p>政策补充文档：</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">文件系统层级标准</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">纯文本</a>]
    <li><a href="debian-policy/upgrading-checklist.html">升级检查清单</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">虚拟软件包列表</a>
    <li><a href="packaging-manuals/menu-policy/">菜单政策</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">纯文本</a>]
    <li><a href="packaging-manuals/perl-policy/">Perl 政策</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">纯文本</a>]
    <li><a href="packaging-manuals/debconf_specification.html">debconf 规范</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacsen 政策</a>
    <li><a href="packaging-manuals/java-policy/">Java 政策</a>
    <li><a href="packaging-manuals/python-policy/">Python 政策</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">版权信息格式规范</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 开发者参考手册" "devref">

<div class="centerblock">
<p>
该手册描述了 Debian 维护者使用的流程和资源。它描述了如何成为一名新的开发者、\
上传流程、如何处理我们的漏洞追踪系统、邮件列表、互联网服务器等。

<p>该手册应当被视为所有 Debian 开发者（无论新手还是老手）的<em>参考手册</em>。

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  ready
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 维护者指南" "debmake-doc">

<div class="centerblock">
<p>
该教程文档描述了普通 Debian 用户和未来开发者们使用 <code>debmake</code>
命令构建 Debian 软件包的内容。
</p>
<p>
它专注于现代的打包风格，并提供了许多简单的实例。
</p>
<ul>
<li>POSIX shell 脚本打包</li>
<li>Python3 脚本打包</li>
<li>使用 Makefile/Autotools/CMake 的 C 语言程序</li>
<li>带有共享链接库的多个二进制包的场景等。.</li>
</ul>
<p>
本篇《Debian 维护者指南》应被视为《Debian 新维护者手册》的后继文档。
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  ready
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 新维护者手册" "maint-guide">

<div class="centerblock">
<p>
本文档尝试使用平常的语言描述普通 Debian 用户（以及未来的开发者）构建 Debian
GNU/Linux 软件包的方法，并给出了工作中的实例。

  <p>和以往的文档不同，本文档基于 <code>debhelper</code>
  和维护者可用的较新工具。作者注意了集合以往各个文档的重要内容。

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  已过时，请使用上文的《Debian 维护者指南》（debmake-doc）
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 打包介绍" "packaging-tutorial">

<div class="centerblock">
<p>
这是 Debian 打包的入门教程。它旨在教授未来的开发者如何修改已有的软件包、\
如何创建自己的软件包以及如何和 Debian 社区合作交流。除了主教程之外，\
它还包括了练习环节以修改 <code>grep</code> 软件包、<code>gnujump</code>
软件包和一个 Java 软件库。
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  已完成
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 菜单系统" "menu">

<div class="centerblock">
<p>
本教程描述了 Debian 菜单系统和 <strong>menu</strong> 软件包。

<p>菜单（menu）软件包的灵感来自旧有的 fvwm2 软件包的 install-fvwm2-menu
程序。在此之上，Debian 菜单尝试提供一个构建菜单的更加通用的界面。通过软件包的
update-menus 命令，其它软件包不再需要为每个 X 窗口管理器进行适配，且该命令\
提供了基于文本和面向 X 的程序的一个通用接口。

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  已完成
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML 在线</a>
  </availability>
</doctable>
</div>

<hr>

<document "Debian 安装器内部详解" "d-i-internals">

<div class="centerblock">
<p>
该文档旨在令新开发者更加熟悉 Debian
安装器，并作为记录技术信息的一个中心文档。

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian 安装器团队">
  <status>
  已完成
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML 在线</a></p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">在线 DocBook XML 源代码</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "dbconfig-common 文档" "dbconfig-common">

<div class="centerblock">
<p>
本文档针对维护依赖运行数据库的软件包的维护者。维护者可以避免手动实现必需的逻辑，\
转而使用 dbconfig-common 在安装、升级、重新配置和解除安装软件包时向用户询问\
特定的问题，并由 dbconfig-common 创建和填充数据库。

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  ready
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  额外还提供了<a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">设计文档</a>。
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  针对依赖运行数据库的软件包的一个政策提议。

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  草稿
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>


#use wml::debian::template title="Informações de segurança" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Mantendo seu sistema Debian seguro</a></li>
<li><a href="#DSAS">Alertas recentes</a></li>
<li><a href="#infos">Fontes das informações de segurança</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> O Debian leva a segurança
muito a sério. Lidamos com todos os problemas de segurança trazidos ao nosso
conhecimento e garantimos que sejam corrigidos dentro de um prazo razoável.</p>
</aside>

<p>
A experiência mostra que a <q>segurança através da obscuridade</q> nunca
funciona. Portanto, a divulgação pública permite soluções mais rápidas e
melhores dos problemas de segurança. A esse respeito, esta página aborda o
status do Debian em relação a várias falhas de segurança conhecidas, que podem
afetar potencialmente o sistema operacional Debian.
</p>

<p>
O projeto Debian coordena muitos alertas de segurança com outros(as)
fornecedores(as) de software livre e, como resultado, esses alertas são
publicados no mesmo dia em que uma vulnerabilidade é tornada pública.
Para receber os alertas de segurança do Debian mais recentes, por favor
inscreva-se na lista de discussão
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
O Debian também participa dos esforços de padronização de segurança:
</p>

<ul>
  <li>Os <a href="#DSAS">alertas de segurança Debian</a> são <a href="cve-compatibility">compatíveis com o CVE</a></li>
  <li>O Debian <a href="oval/">publica</a> seus informativos de segurança usando o <a href="https://github.com/CISecurity/OVALRepo">Open Vulnerability Assessment Language (OVAL)</a></li>
</ul>

<h2><a id="keeping-secure">Mantendo seu sistema Debian seguro</a></h2>

<p>
Os pacotes
<a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
podem ser instalado para manter o computador atualizado com as atualizações de
segurança (e outras) mais recentes automaticamente.

A <a href="https://wiki.debian.org/UnattendedUpgrades">página wiki</a> tem
informações mais detalhadas sobre como configurar manualmente o
<tt>unattended-upgrades</tt>.

<p>
Para mais informações sobre problemas de segurança no Debian, por favor
consulte nossa FAQ e nossa documentação:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ sobre segurança</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">protegendo o Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Alertas recentes</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Estes são os recentes alertas de segurança do Debian (DSA - Debian Security
Advisories) publicados na lista <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> é o link para as informações do
<a href="https://security-tracker.debian.org/tracker">rastreador de segurança Debian</a>,
o número DSA está vinculado ao e-mail de anúncio.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<h2><a id="infos">Fontes das informações de segurança</a></h2>
#include "security-sources.inc"

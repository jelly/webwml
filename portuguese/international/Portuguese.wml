#use wml::debian::template title="Páginas do Debian em Português" NOHEADER="yes"

# This contents of this page is maintained by the portuguese translators.
# Unlike the other pages, this is the original text.
# The translation is in english/international/Portuguese.wml

<h1>Debian em Português</h1>

<p>Esta página traz informações específicas sobre o esforço
de tradução do Debian para o português. A equipe que está empenhada nas
traduções é formada por brasileiros(as) e, por isso, há um grande predomínio
de português brasileiro nas páginas. Essa equipe, que é composta por
desenvolvedores(as) e usuários(as) do Debian, intitula-se
<a href="https://wiki.debian.org/Brasil">Debian Brasil</a>.</p>

<p>Para saber como se inscrever nas listas de discussão para usuários(as)
falantes do português, bem como instruções de como entrar no canal IRC
do projeto, visite as seções
<a href="https://wiki.debian.org/Brasil/Listas">Listas</a> e
<a href="https://wiki.debian.org/Brasil/IRC">IRC</a>
da página do Debian-BR.</p>

<p>Para ter acesso à documentação em português, seja de manuais
traduzidos ou feitos pelos(as) próprios(as) membros(as) da equipe, visite a
seção <a href="https://wiki.debian.org/Brasil/Documentos">
Documentos</a> da página do Debian Brasil.</p>

<p>Para ver as últimas notícias sobre o mundo Debian, em português, visite
a seção <a href="$(HOME)/News/weekly">Novidades</a> da página do Debian.</p>

<p>Para saber informações sobre os grupos de usuários(as) Debian existentes
no Brasil e as atividades que os mesmos têm realizado, visite a seção
<a href="https://wiki.debian.org/Brasil/GUD">grupos de usuários(as) Debian</a>.</p>

<p>Se você <strong>realmente</strong> quer ajudar a desenvolver ainda mais o
Debian, veja algumas das possíveis formas na seção
<a href="https://wiki.debian.org/Brasil/ComoColaborar">como colaborar</a>
ou envie um <a href="mailto:debian-br-geral@alioth-lists.debian.net">e-mail</a>.</p>

<p>Apesar de o nome do projeto ser "Brasil", temos vários(as) amigos(as)
portugueses(as) nas listas de discussão e em nosso canal de IRC, e todos(as)
os(as) usuários(as) falantes do português que quiserem se juntar a nós serão
muito bem-vindos(as).</p>

<p>Obrigado!</p>

<p>Equipe do Debian Brasil &lt;<a href="mailto:debian-br-geral@alioth-lists.debian.net">debian-br-geral@alioth-lists.debian.net</a>&gt;</p>
